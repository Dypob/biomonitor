const options = (buttons, msgChatId, msgId) => { //  Опции перенести в отдельный файл
    return {
        chat_id: msgChatId,
        message_id: msgId,
        parse_mode: 'Markdown',
        disable_web_page_preview: true,
        reply_markup: JSON.stringify({
            inline_keyboard: buttons
        })
    }
};
const weekDays = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

exports.messages = {
    admin: {
        start: {
            text: name => `Привет, ${name}. Что будем делать ?`,
            buttons: (ID, msgID) => {
                return options([
                    [{ text: 'Изменить Расписание', callback_data: 'changeSchedule' }],
                    [{ text: 'Добавить событие', callback_data: 'addAction' }, { text: 'Активные задания', callback_data: 'taskList' }],
                    [{ text: 'Отчеты работников', callback_data: 'reports' }],
                    [{ text: 'Оповестить подчиненных', callback_data: 'notifyMembers' }]
                ], ID, msgID)
            }
        },

        changeSchedule: {
            text: 'Изменить расписание:',
            getName: 'Отправьте имя преподавателя',
            getSchedule: 'Отправьте расписание нового преподавателя',
            succes: 'Отлично! Новый преподаватель добавлен',
            buttons: (users, ID, msgID) => {
                const button = [];

                users.forEach((el, index) => {
                    if (index % 2 == 0) {
                        button.push([{ text: el.name, callback_data: 'selectSchedule ' + el.id }])
                    } else {
                        button[button.length - 1].push({ text: el.name, callback_data: 'selectSchedule ' + el.id })
                    }
                });
                button.push([{ text: '⬅️ Главное меню', callback_data: 'main' }])
                button.unshift([{ text: 'Добавить нового +', callback_data: 'addSchedule' }])
                console.log(button)
                return options(button, ID, msgID)

            }
        },
        userSchedule: {
            text: (name, schedule) => `*Расписание преподователя:* ${name}\n\n${schedule}`,
            buttons: (ID, msgID, scheduleID) => {
                return options([
                    [{ text: 'Удалить', callback_data: 'removeSchedule ' + scheduleID }, { text: 'Изменить', callback_data: 'editSchedule ' + scheduleID }],
                    [{ text: 'Назад', callback_data: 'main' }]
                ], ID, msgID)
            }
        },
        removeSchedule: {
            text: name => `Вы удалили расписание преподавателя ${name}`,
            buttons: (ID, msgID) => {
                return options([
                    [{ text: 'Вернуться в главное меню', callback_data: 'main' }]
                ], ID, msgID)
            }
        },
        editSchedule: {
            text: name => `Отправьте новое расписание для преподавателя ${name}`,
            succes: 'Расписание успешно изменено',
            buttons: (ID, msgID) => options([[{ text: 'Назад', callback_data: 'main' }]], ID, msgID)
        },

        tasks: {
            text: 'Список активных заданий:',
            getTask: 'Отправьте мне название для задания',
            getTaskDesc: 'Отправьте мне описание для задания',
            getTaskDeadLine: 'Отправьте мне максимальный срок для задачи в формате 20.09.2018 (DD.MM.YYYY)',
            dateTextErr: 'Ошибка!\nОтправьте мне максимальный срок для задачи в формате 20.09.2018 (DD.MM.YYYY)',
            succes: 'Отлично! Новый задание добавлено',
            buttons: (tasks, ID, msgID) => {
                const button = [];
                console.log(tasks)
                tasks.forEach((el, index) => {
                    if (index % 2 == 0) {
                        button.push([{ text: el.name, callback_data: 'selectTask ' + el.id }])
                    } else {
                        button[button.length - 1].push({ text: el.name, callback_data: 'selectTask ' + el.id })
                    }
                });

                button.push([{ text: '⬅️ Главное меню', callback_data: 'main' }])
                button.unshift([{ text: 'Добавить новое +', callback_data: 'addTask' }])
                console.log(button)
                return options(button, ID, msgID)

            },
            backButton: (ID, msgID) => options([[{ text: 'Отмена', callback_data: 'main' }]], ID, msgID)

        },
        selectTask: {
            text: (name, desc, daedline) => `Задание ${name}\n\n${desc}\n\nМаксимальный срок: ${daedline}`,
            buttons: (taskID, ID, msgID) => options([[{ text: 'Назад', callback_data: `main` }, { text: 'Удалить', callback_data: `deleteTask ${taskID}` }]], ID, msgID)
        },
        removeTask: {
            text: (name) => `Вы удалили задание ${name}`,
            buttons: (ID, msgID) => options([[{ text: 'Назад', callback_data: 'main' }]], ID, msgID)
        },

        reports: {
            text: (reportList) => {
                let reportsText = reportList.map( item => `[${item.name}](${item.report})\n`).join('');
                return `*Отчеты:*\n\n${reportsText}`
            },
            buttons: (ID, msgID) => options([[{ text: 'Назад', callback_data: 'main' }]], ID, msgID)
        },
        notifyMembers : {
            text: 'Какое сообщение мне отправить вашим подчиненным ?',
            success: 'Отлично! Сообщение отправлено',
            buttons : (ID, msgID) => options([[{text: 'Назад', callback_data: 'main'}]], ID, msgID)
        }
    },
    user: {
        mainMenuBtn: (ID, msgID) => options([[{ text: 'Главное меню', callback_data: 'userMain' }]], ID, msgID),
        start: {
            notDefined: name => `Привет, ${name}\n\nЧтобы продолжить работу со мной, вам следует указать свое настощее имя и фамилию. Это поможет в дальнейшем идентифицировать ваши отчеты о выполненных заданиях.`,
            text: name => `Привет, ${name}. Что тебя интересует ?`,
            buttons: (ID, msgID) => options([[{ text: 'Расписание', callback_data: 'schedule' }, { text: 'Задания', callback_data: 'tasks' }]], ID, msgID)
        },
        schedule: {
            text: 'Выберите преподавателя:',
            scheduleSelected: (name, task) => `Расписание преподавателя ${name}\n\n${task}`,
            buttons: (users, ID, msgID) => {
                const button = [];

                users.forEach((el, index) => {
                    if (index % 2 == 0) {
                        button.push([{ text: el.name, callback_data: 'userSelectSchedule ' + el.id }])
                    } else {
                        button[button.length - 1].push({ text: el.name, callback_data: 'userSelectSchedule ' + el.id })
                    }
                });

                button.push([{ text: 'Назад', callback_data: 'userMain' }]);

                return options(button, ID, msgID);
            },
            backButton: (ID, msgID) => options([[{ text: 'Назад', callback_data: 'schedule' }]], ID, msgID)
        },
        tasks: {
            error : 'Вы должны отправить фотографию!',
            error2: 'Извините, но задание уже выполнено другим пользователем !',
            succes: 'Отлично, отчет отправлен !',
            text: 'Выберите преподавателя:',
            taskSelected: (head, desc, daedline) => `*Задание:* ${head}\n\n${desc}\n\n*Максимальный срок:* ${daedline}`,
            comleted: `Отправьте мне фото подтверждение, о выполненном задании!`,
            buttons: (users, ID, msgID) => {
                const button = [];

                users.forEach((el, index) => {
                    if (index % 2 == 0) {
                        button.push([{ text: el.name, callback_data: 'userSelectTaks ' + el.id }])
                    } else {
                        button[button.length - 1].push({ text: el.name, callback_data: 'userSelectTaks ' + el.id })
                    }
                });

                button.push([{ text: 'Назад', callback_data: 'userMain' }]);

                return options(button, ID, msgID);
            },
            backButton: (taskID, ID, msgID) => options([[{ text: 'Я выполнил(а)', callback_data: `task_completed ${taskID}` }], [{ text: 'Назад', callback_data: 'tasks' }]], ID, msgID)
        }
    }
}


