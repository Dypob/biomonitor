const data = require('./options/config').data;
const messages = require('./messages/messages').messages;
const sqlite3 = require('sqlite3');
const TelegramBot = require('node-telegram-bot-api');
const telegraph = require('telegraph-node');
const fs = require('fs');
const request = require('request');

const bot = new TelegramBot(data.botToken, { polling: true });
const ph = new telegraph();
const db = new sqlite3.Database('./db/myDb', err => {
    if (err) return err

    db.run('CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, userID TEXT, name TEXT, status TEXT DEFAULT user, menuStatus TEXT)');
    db.run('CREATE TABLE IF NOT EXISTS schedules(id INTEGER PRIMARY KEY, name TEXT, schedule TEXT)');
    db.run('CREATE TABLE IF NOT EXISTS reports(id INTEGER PRIMARY KEY, name TEXT, report TEXT)');
    db.run('CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY, name TEXT, desc TEXT DEFAULT noset,deadLine DEFAULT noset)');
    db.run('CREATE TABLE IF NOT EXISTS actions(id INTEGER PRIMARY KEY, date TEXT, action TEXT)');
})

bot.onText(/\/start/, msg => {
    // db.run('INSERT INTO users(userID, name, status) VALUES(?,?,?)', msg.from.id, msg.from.first_name, 'admin')
    const ID = msg.from.id;
    const name = msg.from.first_name;
    db.all(`SELECT status FROM users WHERE userID = ${ID}`, (err, row) => {
        if (row.length == 0) {
            db.run('INSERT INTO users(userID, status, menuStatus) VALUES(?,?,?)', ID, 'notDefined', 'getName');
            bot.sendMessage(ID, messages.user.start.notDefined(msg.from.first_name));
        } else {
            if (row[0].status == 'admin') {
                db.run('DELETE FROM schedules WHERE schedule = ?', 'noset');
                db.run('DELETE FROM tasks WHERE desc = ? OR deadLine = ?', 'noset', 'noset');
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', 'start', ID);
                bot.sendMessage(ID, messages.admin.start.text(name), messages.admin.start.buttons());
            } else if (row[0].status == 'member') {
                bot.sendMessage(ID, messages.user.start.text(name), messages.user.start.buttons());
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', 'start', ID);
            } else {
                bot.sendMessage(ID, messages.user.start.notDefined(name));
            }
        }
    })
})

bot.on('callback_query', query => {
    const data = query.data.split(' ');
    console.log(data)
    const ID = query.from.id;
    const msgID = query.message.message_id;

    switch (data[0]) {
        case 'main':
            db.run('DELETE FROM tasks WHERE desc = ? OR deadLine = ?', 'noset', 'noset');
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'main', ID);
            bot.editMessageText(messages.admin.start.text(query.from.first_name), messages.admin.start.buttons(ID, msgID))
            break;
        case 'addSchedule':
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', query.data, ID)
            bot.sendMessage(ID, messages.admin.changeSchedule.getName)
            break;
        case 'changeSchedule':
            db.all(`SELECT * FROM schedules ORDER BY name`, (err, row) => {
                bot.editMessageText(messages.admin.changeSchedule.text, messages.admin.changeSchedule.buttons(row, ID, msgID))
            })
            break;
        case 'selectSchedule':
            db.all(`SELECT * FROM schedules WHERE ID = ${data[1]}`, (err, row) => {
                bot.editMessageText(messages.admin.userSchedule.text(row[0].name, row[0].schedule), messages.admin.userSchedule.buttons(ID, msgID, row[0].id))
            })
            break;
        case 'removeSchedule':
            db.all(`SELECT name FROM schedules WHERE ID = ${data[1]}`, (err, row) => {
                db.run('DELETE FROM schedules WHERE ID = ?', data[1]);
                bot.editMessageText(messages.admin.removeSchedule.text(row[0].name), messages.admin.removeSchedule.buttons(ID, msgID))
            })
            break;
        case 'editSchedule':
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', `editSchedule ${data[1]}`, ID);
            db.all(`SELECT name FROM schedules WHERE ID = ${data[1]}`, (err, row) => {
                bot.editMessageText(messages.admin.editSchedule.text(row[0].name), messages.admin.editSchedule.buttons(ID, msgID))
            })
            break;
        case 'taskList':
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', query.data, ID)
            db.all(`SELECT * FROM tasks`, (err, row) => {
                bot.editMessageText(messages.admin.tasks.text, messages.admin.tasks.buttons(row, ID, msgID))
            })
            break;
        case 'addTask':
            // console.log('1')
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', 'addTask', ID);
            bot.editMessageText(messages.admin.tasks.getTask, messages.admin.tasks.backButton(ID, msgID))
            break;

        case 'selectTask':
            db.all(`SELECT * FROM tasks WHERE ID = ${data[1]}`, (err, row) => {
                bot.editMessageText(messages.admin.selectTask.text(row[0].name, row[0].desc, row[0].deadLine), messages.admin.selectTask.buttons(row[0].id, ID, msgID))
            })
            break;
        case 'deleteTask':
            db.all(`SELECT name FROM tasks WHERE ID = ${data[1]}`, (err, row) => {
                db.run('DELETE FROM tasks WHERE ID = ?', data[1]);
                bot.editMessageText(messages.admin.removeTask.text(row[0].name), messages.admin.removeTask.buttons(ID, msgID))
            })
            break;
        case 'reports':
            db.all('SELECT * FROM reports', (err, row) => {
                bot.editMessageText(messages.admin.reports.text(row), messages.admin.reports.buttons(ID, msgID))
            })
            break;
        case 'notifyMembers':
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', 'notifyMembers', ID);
            bot.sendMessage(ID, messages.admin.notifyMembers.text);
            break;

        // user 
        case 'userMain':
            bot.editMessageText(messages.user.start.text(query.from.first_name), messages.user.start.buttons(ID, msgID));
            break
        case 'schedule':
            db.all(`SELECT * FROM schedules ORDER BY name`, (err, row) => {
                bot.editMessageText(messages.user.schedule.text, messages.user.schedule.buttons(row, ID, msgID))
            })
            break;
        case 'userSelectSchedule':
            db.all(`SELECT * FROM schedules WHERE id = ${data[1]}`, (err, row) => {
                // console.log(row)
                bot.editMessageText(messages.user.schedule.scheduleSelected(row[0].name, row[0].schedule), messages.user.schedule.backButton(ID, msgID))
            })
            break;
        case 'tasks':
            db.all(`SELECT * FROM tasks`, (err, row) => {
                bot.editMessageText(messages.user.tasks.text, messages.user.tasks.buttons(row, ID, msgID))
            })
            break;
        case 'userSelectTaks':
            db.all(`SELECT * FROM tasks WHERE id = ${data[1]}`, (err, row) => {
                bot.editMessageText(messages.user.tasks.taskSelected(row[0].name, row[0].desc, row[0].deadLine), messages.user.tasks.backButton(row[0].id, ID, msgID))
            })
            break;
        case 'task_completed':
            db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', `comlpeteTask ${data[1]}`, ID);
            bot.editMessageText(messages.user.tasks.comleted, messages.user.mainMenuBtn(ID, msgID), messages.admin.notifyMembers(ID, msgID))
            break;
    }
});

bot.on('message', msg => {
    console.log(msg)
    if (msg.text == "/start") return false

    db.all(`SELECT menuStatus FROM users WHERE userID = ${msg.from.id}`, (err, row) => {
        let callbackData = row[0].menuStatus.split(' ');
        switch (callbackData[0]) {
            case 'addSchedule':
                bot.sendMessage(msg.from.id, messages.admin.changeSchedule.getSchedule);
                db.run('INSERT INTO schedules(name, schedule) VALUES(?, ?)', msg.text, 'noset');
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'addScheduleDesc', msg.from.id);
                break;
            case 'addScheduleDesc':
                bot.sendMessage(msg.from.id, messages.admin.changeSchedule.succes, messages.admin.editSchedule.buttons());
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'start', msg.from.id);
                db.run('UPDATE schedules SET schedule = ? WHERE schedule = ?', msg.text, 'noset');
                break;
            case 'editSchedule':
                db.all(`SELECT menuStatus FROM users WHERE userID = ${msg.from.id}`, (err, row) => {
                    let data = row[0].menuStatus.split(" ");
                    db.run('UPDATE schedules SET schedule = ? WHERE ID = ?', msg.text, data[1]);
                })
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'main', msg.from.id);
                bot.sendMessage(msg.from.id, messages.admin.editSchedule.succes, messages.admin.editSchedule.buttons())
                break;
            case 'addTask':
                console.log(1)
                db.run('INSERT INTO tasks(name) VALUES(?)', msg.text);
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'addTaskDesc', msg.from.id);
                bot.sendMessage(msg.from.id, messages.admin.tasks.getTaskDesc, messages.admin.tasks.backButton());
                break;
            case 'addTaskDesc':
                console.log(2)
                db.run('UPDATE tasks SET desc = ? WHERE desc = ?', msg.text, 'noset');
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ?', 'addTaskDeadLine', msg.from.id)
                bot.sendMessage(msg.from.id, messages.admin.tasks.getTaskDeadLine);
                break;
            case 'addTaskDeadLine':
                let deadLine = msg.text.split('.').reverse('').join('-');
                if (Date.parse(deadLine)) {
                    db.run('UPDATE tasks SET deadLine = ? WHERE deadLine = ?', deadLine, 'noset');
                    bot.sendMessage(msg.from.id, messages.admin.tasks.succes, messages.admin.editSchedule.buttons())
                } else {
                    bot.sendMessage(msg.from.id, messages.admin.tasks.dateTextErr);
                }
                break;
            case 'notifyMembers':
                db.run('UPDATE users SET menuStatus = ? WHERE userID = ? ', 'start', msg.from.id);
                db.each(`SELECT userID FROM users WHERE userID != ${msg.from.id}`, (err, row) =>{
                    bot.sendMessage(row.userID, msg.text)
                })
                bot.sendMessage(msg.from.id, messages.admin.notifyMembers.success, messages.admin.notifyMembers.buttons());
                break;
            //user

            case 'getName':
                db.run('UPDATE users SET status = ?, name = ?, menuStatus = ? WHERE userID = ?', 'member', msg.text, 'start', msg.from.id);
                bot.sendMessage(msg.from.id, 'Окей, ' + msg.text);
                break;

            case 'comlpeteTask':
                if (msg.photo) {
                    let fileID = msg.photo[msg.photo.length - 1].file_id;
                    console.log(fileID);

                    let path = bot.downloadFile(fileID, './testimg')
                        .then(path => {
                            var formData = {
                                file: {
                                    value: fs.createReadStream(path), //file_path  пут файла
                                    options: {
                                        filename: path.replace(`testimg`, ''),
                                        contentType: 'image/jpeg'
                                    }
                                }
                            }

                            request.post({ url: 'https://telegra.ph/upload', formData: formData }, function optionalCallback(err, httpResponse, body) {
                                if (err) {
                                    return console.error('upload failed:', err);
                                }

                                let bodyParsed = JSON.parse(body);
                                db.all(`SELECT name FROM tasks WHERE id = ${callbackData[1]}`, (err, tasks) => {
                                    db.all(`SELECT name FROM users WHERE userID = ${msg.from.id}`, (err, user) => {
                                        try {
                                            ph.createPage(data.telegraphToken, `Отчет по заданию "${tasks[0].name}" (Выполнил: ${user[0].name})`, [{ tag: 'h1', children: [msg.caption ? msg.caption : ""] }, { tag: 'img', attrs: { src: bodyParsed[0].src } }], {
                                                return_content: true
                                            }).then((result) => {
                                                db.run('INSERT INTO reports(name,report) VALUES(?,?)', user[0].name, result.url);
                                                db.run(`DELETE FROM tasks WHERE id = ${callbackData[1]}`);
                                                bot.sendMessage(msg.from.id, messages.user.tasks.succes);
                                            })
                                        } catch (error) {
                                            bot.sendMessage(msg.from.id, messages.user.tasks.error2)
                                        }
                                    })
                                })
                            });
                        })


                } else {
                    bot.sendMessage(msg.from.id, messages.user.tasks.error)
                }
                break;
        }
    })
})

// const telegraph = require('telegraph-node')
// const ph = new telegraph();
// const token = '';
// ph.createPage(data.telegraphToken, 'Отчет от Дмитрия Николаева', [{tag: 'img', attrs: {src: "https://img.tglab.uz/397269078/15369529955b9c0aa3b200d.jpg"}}], {
//   return_content: true
// }).then((result) => {
// console.log(result)
// 




