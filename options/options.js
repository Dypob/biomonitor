exports.options = (buttons, msgChatId, msgId) => { //  Опции перенести в отдельный файл
  return {
    chat_id: msgChatId,
    message_id: msgId,
    parse_mode: 'Markdown',
    disable_web_page_preview: false,
    reply_markup: JSON.stringify({
      inline_keyboard: buttons
    })
  }
};